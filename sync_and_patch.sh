#!/bin/bash

filepath=`pwd`

cd $filepath
cd sources
git clone git://git.yoctoproject.org/poky -b daisy ./poky
cd poky
git checkout ccd470ba5fd739d3ca7c0e74c828642e9b9828c7
cp $filepath/sources/meta-onbox/patches/autotools-brokensep.bbclass $filepath/sources/poky/meta/classes
cp $filepath/sources/meta-onbox/patches/kernel.bbclass $filepath/sources/poky/meta/classes
cp $filepath/sources/meta-onbox/patches/powertop_2.5.bb $filepath/sources/poky/meta/recipes-kernel/powertop/powertop_2.5.bb
cd $filepath/sources/
git clone git://git.openembedded.org/meta-openembedded -b daisy ./meta-openembedded
cd $filepath/sources/meta-openembedded
git checkout d3d14d3fcca7fcde362cf0b31411dc4eea6d20aa
cd $filepath/sources/
git clone git://git.yoctoproject.org/meta-fsl-arm -b daisy ./meta-fsl-arm
cd $filepath/sources/meta-fsl-arm
git checkout 0c4de80867c3ab4e9682dd7802d3fd907d1e1a23
cd $filepath/sources/
git clone http://bitbucket.org/2bitmicro/meta-fsl-arm-extra.git
cd $filepath/sources/meta-fsl-arm-extra
git checkout bdde52e7ad0e8fd0ad9bcee2626a7c9ae398367e
cd $filepath/sources/
git clone git://github.com/meta-qt5/meta-qt5
cd $filepath/sources/meta-qt5
git checkout 23405d3a66c308e0b2ea64308b834285850c5c9f
cd $filepath/sources/meta-onbox/patches
if [ ! -d $filepath/build ]; then
mkdir $filepath/build
fi
if [ ! -d $filepath/build/conf ]; then
mkdir $filepath/build/conf
fi
if [ -d $filepath/build/conf ]; then
cp bblayers.conf $filepath/build/conf/
cp local.conf $filepath/build/conf/
fi
cd $filepath
MACHINE=cfa100xx source ./setup-environment build

export PATH=$PATH:$filepath/sources/poky/bitbake/bin/

cd $filepath/build
