# Disable services that are enabled by default, but not necessary for an 
# image to be useful (want in the image, but not running by default)

deploy_manifests () {
	cd ${DEPLOY_DIR}/licenses
	cp ${IMAGE_NAME}/license.manifest ${IMAGE_ROOTFS}/etc/license.manifest
	cp ${IMAGE_NAME}/package.manifest ${IMAGE_ROOTFS}/etc/package.manifest
}

remove_qtdemo_stuffs () {
	rm ${IMAGE_ROOTFS}/etc/init.d/qtdemo
	rm ${IMAGE_ROOTFS}/etc/rc5.d/S99qtdemo
}

fixup_hostname () {
	rm ${IMAGE_ROOTFS}/etc/hostname
	echo onbox > ${IMAGE_ROOTFS}/etc/hostname
}

rename_kernel_and_devtree_links () {
	cd ${IMAGE_ROOTFS}/boot
	rm ${IMAGE_ROOTFS}/boot/zImage-onbox
	rm ${IMAGE_ROOTFS}/boot/oftree-cfa10036
	ln -s zImage-3.12.42 zImage-cfa10036
	ln -s devicetree-zImage-imx28-onbox.dtb oftree-cfa10036
}


copy_etc_to_root_for_restore_to_defaults () {
	cp -R ${IMAGE_ROOTFS}/etc/* ${IMAGE_ROOTFS}/home/root/.config/etc
	cd ${IMAGE_ROOTFS}/etc/init.d/
	ln -s /etc/init.d/reset-monitor.sh ../rc5.d/S99reset-monitor.sh
}

# deploy manifest files to the target
ROOTFS_POSTPROCESS_COMMAND += "deploy_manifests ;"
ROOTFS_POSTPROCESS_COMMAND += "remove_qtdemo_stuffs ;"
ROOTFS_POSTPROCESS_COMMAND += "rename_kernel_and_devtree_links ;"
ROOTFS_POSTPROCESS_COMMAND += "fixup_hostname ;"
ROOTFS_POSTPROCESS_COMMAND += "copy_etc_to_root_for_restore_to_defaults ;"


