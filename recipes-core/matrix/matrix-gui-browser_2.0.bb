DESCRIPTION = "Simple Qt web display using webkit"
HOMEPAGE = "http://bitbucket.org/2bitmicro/matrix_browser"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://main.cpp;beginline=9;endline=37;md5=884b90f5bf0d711fe32c4f04b5276496"
SECTION = "multimedia"

inherit qt-provider

SRC_URI[md5sum] = "f2541fd85ef9cd3637ba1de2ca31c2c7"
SRC_URI[sha256sum] = "6b20cbbe48ef4712500cc26ae38563ce770e17f1c9363cdd74aac6f57dc4e171"
SRC_URI[md5sum] = "bcee7cb79eca9aad0f2f1340bae12cff"
SRC_URI[sha256sum] = "64d46c370ff0ebea12daefda08c34aebcad61a63b12b9fa1ddc28015ed309cdb"

# Make sure that QT font libraries have been installed
RDEPENDS_${PN} += "${QT_RDEPENDS_FONTS}"

DEPENDS += "${QT_DEPENDS_WEBKIT}"

PR = "r0"

SRCREV = "78bfae16d5a027836a82c199efdfb94c4478e6f6"

SRC_URI = "git://bitbucket.org/2bitmicro/matrix_browser.git;protocol=http"

#SRC_URI += "${@base_conditional('QT_PROVIDER', 'qt5', 'file://qt5-webkit.patch file://qt5-gui-widgets-move.patch', '', d)}"
SRC_URI += "${@base_conditional('QT_PROVIDER', 'qt5', 'file://qt5-gui-widgets-move.patch', '', d)}"

S = "${WORKDIR}/git"

do_install() {
	install -d ${D}/${bindir}
	install -m 0755 ${B}/matrix_browser ${D}/${bindir}
}
