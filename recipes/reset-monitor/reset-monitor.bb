DESCRIPTION = "Onbox Reset to defaults button monitor"
PR = "r0"
LICENSE = "MIT"
RM_WORK_EXCLUDE += "reset-monitor"

SRC_URI = "file://reset-monitor.c \
					 file://reset-monitor.sh \
           file://LICENSE \
          "

LIC_FILES_CHKSUM = "file://${FILE_DIRNAME}/files/LICENSE;md5=a886c9ef769b2d8271115d2502512e5d"
          
do_compile() {
    ${CC} ${CFLAGS} ${LDFLAGS} ${WORKDIR}/reset-monitor.c -o reset-monitor
}

do_install() {
    install -m 0755 -d ${D}${bindir}
    install -m 0755 -d ${D}/etc/init.d
    install -m 0755 ${S}/reset-monitor ${D}${bindir}
    install -m 0755 ${FILE_DIRNAME}/files/reset-monitor.sh ${D}/etc/init.d/reset-monitor.sh
}

pkg_postinst_reset-monitor () {
#!/bin/sh -e
# Commands to carry out
	if [ ! -e $D/home/root/.config ]; then
		mkdir $D/home/root/.config
	fi
	if [ ! -e $D/home/root/.config/etc ]; then
		mkdir $D/home/root/.config/etc
	fi
}

