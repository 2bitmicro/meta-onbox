/*
 * reset monitor
 *
 * Monitors a given input for a press event.  The input must have auto-
 * repeat on for this to work properly.  Waits for 10 seconds, then it
 * makes a backup copy of /etc, and pushes a clean /etc to the root.
 * 
 * Copyright (C) 2015 Brian Lilly <brian@2bitmicro.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>
#include <time.h>                // for gettimeofday()

static const char *const evval[3] = {
    "RELEASED",
    "PRESSED ",
    "REPEATED"
};

int main(void)
{
	const char *dev = "/dev/input/by-path/platform-gpio_keys.8-event";
	struct input_event ev;
	ssize_t n;
	int fd;
	time_t start, end;
	double elapsed = 0;
	unsigned char start_timer = 0;
	unsigned char key_depressed = 0;

	fd = open(dev, O_RDONLY);
	if (fd == -1) 
	{
		fprintf(stderr, "Cannot open %s: %s.\n", dev, strerror(errno));
		return EXIT_FAILURE;
	}

	while (1) 
	{
		time(&end);

		elapsed = difftime(end, start);

		if(key_depressed)
		{
			if(elapsed > 10)
			{
				system("if [ -e /home/root/.config/etc_b4_reset ]; then rm -rf /home/root/.config/etc_b4_reset && echo previous /etc removed;fi");
				usleep(100000);
				system("busybox mv /etc /home/root/.config/etc_b4_reset && echo old etc preserved;");
				usleep(100000);
				system("busybox cp -R /home/root/.config/etc /etc && echo original etc restored;");
				usleep(100000);
				system("if [ ! -L /etc/rc5.d/S99reset-monitor ]; then ln -s /etc/init.d/reset-monitor.sh /etc/rc5.d/S99reset-monitor && echo reset-monitor re-linked;fi");
				printf("copied folder, rebooting\n");
				usleep(100000);
				start_timer = 0;
				key_depressed = 0;
				system("reboot&");
				exit(1);				
			}
		} 

		usleep(1);

		n = read(fd, &ev, sizeof ev);
		if (n == (ssize_t)-1) 
		{
			if (errno == EINTR)
				continue;
			else
				break;
		} 
		else
		{
			if (n != sizeof ev) 
			{
				errno = EIO;
				continue;
			}
		}
		
		if (ev.type == EV_KEY && ev.value >= 0 && ev.value <= 2)
		{
			if(ev.value == 1)
			{
				time(&start);  /* start the timer */
				start_timer = 1;
				key_depressed = 1;
			}
			else if(ev.value == 0)
			{
				start_timer = 0;
				key_depressed = 0;
			}
			else if(ev.value == 2)
				key_depressed = 1;
		}
	}

	fflush(stdout);
	fprintf(stderr, "%s.\n", strerror(errno));
	return EXIT_FAILURE;
}
