#!/bin/sh
### BEGIN INIT INFO
# Provides:             onbox-reset-monitor
# Required-Start:
# Required-Stop:
# Default-Start:        S
# Default-Stop:
### END INIT INFO

export USER="root"

/usr/bin/reset-monitor &
echo starting reset monitor | tee /dev/kmsg > /dev/null
