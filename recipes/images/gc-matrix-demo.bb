DESCRIPTION = "Matrix Image for Global Cache boards"
LICENSE = "MIT"

PR = "pr8"

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_INSTALL += " linux-firmware kernel-modules init-ifupdown iw libpng busybox-udhcpd"

EXTRA_IMAGE_FEATURES += " \
	nfs-server \
	ssh-server-dropbear \
	package-management \
	hwcodecs \
	tools-debug \
	tools-profile \
	dev-pkgs \
	tools-sdk \
"

IMAGE_INSTALL += " devmem2 wireless-tools i2c-tools tzdata wpa-supplicant"
IMAGE_INSTALL += " nano screen"
IMAGE_INSTALL += " openssh-sftp openssh-sftp-server"

IMAGE_LINGUAS = " "

IMAGE_INSTALL += " \
	cpufrequtils \
	nano \
	packagegroup-core-qt4e \
	qt4-embedded-demos \
	qt4-embedded-examples \
	qt4-embedded-dev \
	qt4-embedded \
	packagegroup-qte-toolchain-target \
	devmem2 \
	curl \
	qt4-embedded-plugin-imageformat-gif \
	qt4-embedded-plugin-imageformat-jpeg \
	qt4-embedded-qml-plugins \
	matrix-gui \
	matrix-gui-browser \
	matrix-gui-settings-demos \
	reset-monitor \
"

inherit core-image gc_image cfa_smart_config license image debian
